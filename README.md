About Image Server
==================

Image server is the best place to keep and serve your images for your website. The best feature is it automatically allows you to request any image at any resolution, and Image Server handles all the gory details regarding image resizing and caching.  It just works!



TODO
====

- decide the structure of the project
	- will need a public facing URL for serving images (perhaps /img/IMAGE_NAME)
		- optionally specify a desired resolution (perhaps /img/IMAGE_NAME/300x300)
		- for width only, something like /img/IMAGE_NAME/300x
		- for height only, something like /img/IMAGE_NAME/x300
		- perhaps we would make an "img" controller to serve images
		- it needs to check for a cached version of the image at the given resolution
		- it needs to resize images if there is no cached version (and store it for future requests)
		- if the original image is newer than the cached version, delete that image's cache directory
	- will need a backend area where you can manage images.
		- simply display all images and allow you to delete, rename, view cached versions, and clear cache
		- just in case there are tons of images, support pagination
		- people will want to upload them into directories, so support directories here.
		- in the backend area, integrate with Facebook, Twitter, Imagur APIs to get images from those sources.
			- this will download the image from the service to the server and store it with all the other images
		- provide an option to search on the image name (and possibly keywords and descriptions (see below))
	- if there is a database, it would be nice to specify keywords and a description of each image for easy searching.
		- if this happens, integrate the keyword / description search into the search in the backend image manager
	- in a future version, it could provide its own web service so that other websites could actually perform searches across all images
- decide how to collaborate; could use trello or any other project management service


Contributors
============

- Matt Larson (larsomat@uwec.edu)
- Andrew Petz (petza@uwec.edu)